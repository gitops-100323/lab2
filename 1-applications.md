# lab2
## Kustomization
```
mkdir -p clusters/default/applications/app1
cat <<EOF > clusters/default/applications/app1/kustomization.yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
- sync.yaml
EOF

cat <<EOF > clusters/default/applications/app1/sync.yaml
apiVersion: kustomize.toolkit.fluxcd.io/v1beta2
kind: Kustomization
metadata:
  name: app1
  namespace: flux-system
spec:
  interval: 1m0s
  path: ./applications/app1
  prune: true
  sourceRef:
    kind: GitRepository
    name: flux-system
EOF
```
## Deployment
```
mkdir -p applications/app1
cat <<EOF > applications/app1/deployment.yaml
---
apiVersion: v1
kind: Namespace
metadata:
  name: app1
---
apiVersion: v1
kind: Service
metadata:
  name: podinfo
  namespace: app1
spec:
  type: LoadBalancer
  selector:
    app: podinfo
  ports:
    - name: http
      port: 9898
      protocol: TCP
      targetPort: http
    - port: 9999
      targetPort: grpc
      protocol: TCP
      name: grpc
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: podinfo
  namespace: app1
  annotations:
    secret.reloader.stakater.com/reload: "prod-secrets"
spec:
  minReadySeconds: 3
  revisionHistoryLimit: 5
  progressDeadlineSeconds: 60
  strategy:
    rollingUpdate:
      maxUnavailable: 0
    type: RollingUpdate
  selector:
    matchLabels:
      app: podinfo
  template:
    metadata:
      annotations:
        prometheus.io/scrape: "true"
        prometheus.io/port: "9797"
      labels:
        app: podinfo
    spec:
      containers:
      - name: podinfod
        image: ghcr.io/stefanprodan/podinfo:6.1.8
        imagePullPolicy: IfNotPresent
        ports:
        - name: http
          containerPort: 9898
          protocol: TCP
        - name: http-metrics
          containerPort: 9797
          protocol: TCP
        - name: grpc
          containerPort: 9999
          protocol: TCP
        command:
        - ./podinfo
        - --port=9898
        - --port-metrics=9797
        - --grpc-port=9999
        - --grpc-service-name=podinfo
        - --level=info
        - --random-delay=false
        - --random-error=false
        env:
        - name: PODINFO_UI_COLOR
          value: "#34577c"
        # - name: PODINFO_UI_COLOR
        #   valueFrom:
        #     secretKeyRef:
        #       name: prod-secrets
        #       key: podinfoUIColor
        #       optional: false
        livenessProbe:
          exec:
            command:
            - podcli
            - check
            - http
            - localhost:9898/healthz
          initialDelaySeconds: 5
          timeoutSeconds: 5
        readinessProbe:
          exec:
            command:
            - podcli
            - check
            - http
            - localhost:9898/readyz
          initialDelaySeconds: 5
          timeoutSeconds: 5
        resources:
          limits:
            cpu: 2000m
            memory: 512Mi
          requests:
            cpu: 100m
            memory: 64Mi
EOF
```