# lab2
## create policy for IAM read secret and attach kustomization-controller(read kms) to this IAM
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "secretsmanager:ListSecrets",
                "secretsmanager:GetSecretValue"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
```
## AWS Access Key
```
echo -n 'Qxxxxxx' | base64
echo -n 'Yxxxxxx' | base64

```
```
cat <<EOF > security/sops-kms-yaml/awssm-secret.yaml
apiVersion: v1
data:
    access-key: base64-key
    secret-access-key: base64-key
kind: Secret
metadata:
    name: awssm-secret
    namespace: default
type: Opaque
EOF
```
