# lab2
## Kustomization
```
mkdir -p clusters/default/applications/app2
cat <<EOF > clusters/default/applications/app2/kustomization.yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
- sync.yaml
EOF

cat <<EOF > clusters/default/applications/app2/sync.yaml
apiVersion: kustomize.toolkit.fluxcd.io/v1beta2
kind: Kustomization
metadata:
  name: app2
  namespace: flux-system
spec:
  interval: 1m0s
  path: ./applications/app2
  prune: true
  sourceRef:
    kind: GitRepository
    name: flux-system
EOF
```
## Deployment
```
mkdir -p applications/app2
cat <<EOF > applications/app2/deployment.yaml
---
apiVersion: v1
kind: Namespace
metadata:
  name: app2
---
apiVersion: v1
kind: Service
metadata:
  name: app2
  namespace: app2
spec:
  type: ClusterIP
  selector:
    app: app2
  ports:
    - name: http
      port: 1323
      protocol: TCP
      targetPort: http
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: app2
  namespace: app2
  annotations:
    secret.reloader.stakater.com/reload: "postgrespwd"
spec:
  # minReadySeconds: 3
  # revisionHistoryLimit: 5
  # progressDeadlineSeconds: 60
  strategy:
    rollingUpdate:
      maxUnavailable: 0
    type: RollingUpdate
  selector:
    matchLabels:
      app: app2
  template:
    metadata:
      labels:
        app: app2
    spec:
      containers:
      - name: app2
        image: 290913681714.dkr.ecr.ap-southeast-1.amazonaws.com/gitops/go-postgres:v1
        imagePullPolicy: IfNotPresent
        ports:
        - name: http
          containerPort: 1323
          protocol: TCP
        env:
        - name: DB_PASSWORD
          valueFrom:
            secretKeyRef:
              name: postgrespwd
              key: password
        - name: DB_HOST
          value: "db-1.c1tlihufxxtp.ap-southeast-1.rds.amazonaws.com"
        - name: DB_NAME
          value: "clean-lab"
        - name: DB_PORT
          value: "5432"
        - name: DB_USER
          value: "postgres"
        resources:
          limits:
            cpu: 200m
            memory: 256Mi
          requests:
            cpu: 10m
            memory: 16Mi
EOF
```
## postgres secret
```
cat <<EOF > platform/external-secrets/postgrespwd.yaml
apiVersion: external-secrets.io/v1beta1
kind: ExternalSecret
metadata:
  name: postgrespwd
  namespace: app2
spec:
  refreshInterval: 1m
  secretStoreRef:
    name: example-store
    kind: ClusterSecretStore
  target:
    name: postgrespwd
    creationPolicy: Owner
  data:
  - secretKey: password
    remoteRef:
      key: postgres-secret
      property: password
EOF
```